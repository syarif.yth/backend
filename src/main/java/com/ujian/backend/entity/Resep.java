package com.ujian.backend.entity;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@ToString
public class Resep {
    private List<Obat> obat;

    public Resep() {
        this.obat = new ArrayList<Obat>();
    }

    public void addObat(Obat obat) {
        this.obat.add(obat);
        // obat.add("Decolgen");
    }

    public List<Obat> viewObat() {
        return obat;
    }

    public boolean hasObatInResep(String namaObat) {
        for(int i = 0; i < this.obat.size(); i++) {
            if (this.obat.get(i).getObat() == namaObat) {
                return true;
            }
        }
        return false;
    }
}
