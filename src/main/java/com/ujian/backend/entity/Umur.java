package com.ujian.backend.entity;

import io.micrometer.observation.annotation.Observed;

import java.util.List;

public class Umur {

    public int year;

    public int month;

    public int day;


    public Umur(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "Umur{" +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                '}';
    }

}
