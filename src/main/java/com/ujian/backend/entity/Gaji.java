package com.ujian.backend.entity;

import java.util.List;

public class Gaji {
    public int employee;
    public List komponengaji;
    public Gaji(int employee, List komponengaji)
    {
        this.employee = employee;
        this.komponengaji = komponengaji;
    }

    public int getEmployee() {
        return employee;
    }

    public List getKomponengaji() {
        return komponengaji;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }

    public void setKomponengaji(List komponengaji) {
        this.komponengaji = komponengaji;
    }
}
