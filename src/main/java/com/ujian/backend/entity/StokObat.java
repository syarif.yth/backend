package com.ujian.backend.entity;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StokObat {

    private List<Obat> listObat;

    public StokObat() {
        this.listObat = new ArrayList<Obat>();
        List<String> kandunganProris = new ArrayList<String>();
        kandunganProris.add("Ibuprofen");
        List<String> kandunganBodrex = new ArrayList<String>();
        kandunganBodrex.add("Paracetamol");
        List<String> kandunganParatusin = new ArrayList<String>();
        kandunganParatusin.add("Paracetamol");
        kandunganParatusin.add("pseudoepedrid");
        kandunganParatusin.add("noscapine");
        kandunganParatusin.add("ctm");
        kandunganParatusin.add("guafenisin");
        kandunganParatusin.add("succus liquiritae");
        kandunganParatusin.add("ethanol");

        this.listObat.add(new Obat("Proris", kandunganProris));
        this.listObat.add(new Obat("Bodrex", kandunganBodrex));
        this.listObat.add(new Obat("Paratusin", kandunganParatusin));
    }

    public List<Obat> getListObat() {
        return this.listObat;
    }

    public List<Obat> filterByKandunganObat(String kandunganObat) {

        List<Obat> result = new ArrayList<Obat>();

        for (int i = 0; i < this.listObat.size(); i++) {
            Obat obat = this.listObat.get(i);
            List<String> kandObat = obat.getKandunganObat();
            for (int j = 0; j < kandObat.size(); j++) {
                String kandungan = kandObat.get(j);
                if (kandungan.toLowerCase().contains(kandunganObat.toLowerCase())) {
                    result.add(obat);
                }
            }
        }

        return result;
    }

    public List<Obat> findObat(String namaObat) {
        List<Obat> result = new ArrayList<Obat>();

        for (int i = 0; i < this.listObat.size(); i++) {
            Obat obat = this.listObat.get(i);
            if (obat.getObat().toLowerCase().contains(namaObat.toLowerCase())) {
                result.add(obat);
            }
        }

        return result;
    }
}
