package com.ujian.backend.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RestController
public class BookingKamarOperasi
{
    @RequestMapping(value = "/bookingkamaroperasi/{bookingDate}",
            method = RequestMethod.GET,
            produces = "Application/JSON")
    @ResponseBody
    public static String booking(@PathVariable String bookingDate)
    {
        return bookingDate;
    }

}
