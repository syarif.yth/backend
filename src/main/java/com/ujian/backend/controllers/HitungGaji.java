package com.ujian.backend.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ujian.backend.entity.Ptkp;
import com.ujian.backend.entity.ResultUmur;
import com.ujian.backend.entity.Umur;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RestController
public class HitungGaji {
    @RequestMapping(value = "/hitunggaji",
            method = RequestMethod.GET,
            produces = "Application/JSON")
    @ResponseBody
    public static String hitung()
    {
        try {
            Ptkp ptkp = new Ptkp();
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(ptkp);
        } catch (Exception e) {
            throw new Error(e);
        }
    }

}
