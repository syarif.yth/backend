package com.ujian.backend.controllers;

import com.ujian.backend.dto.RequestBodyAlergi;
import com.ujian.backend.entity.Obat;
import com.ujian.backend.entity.StokObat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class ValidasiAlergiObat {
    public List<Obat> resultResep;

    @RequestMapping(path = "/validasialergiobat",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Obat> validasiAlergiObat(@RequestBody RequestBodyAlergi requestAlergi)
    {
        StokObat stokObat = new StokObat();

        for (int i = 0; i < (requestAlergi.getPasien().getAllergies().size()); i++) {
            String allergy = requestAlergi.getPasien().getAllergies().get(i);
            this.resultResep = stokObat.filterByKandunganObat(allergy);
        }
        return this.resultResep;
    }
}

