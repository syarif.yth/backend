package com.ujian.backend.dto;

import com.ujian.backend.entity.Obat;
import com.ujian.backend.entity.Pasien;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RequestBodyAlergi {
    private Pasien pasien;
    private List<Obat> resep;


}
